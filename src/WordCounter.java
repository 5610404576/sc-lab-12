import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class WordCounter {
	private String filename;
	private HashMap<String, Integer> wordCount = new HashMap<String, Integer>();

	public WordCounter(String filename) {
		this.filename = filename;
	}

	public void count() {
		String[] words = readFile().split(" ");
		for(String word:words){
			if(this.wordCount.containsKey(word)){
				int currentCount = this.wordCount.get(word);
				this.wordCount.replace(word, ++currentCount);
			}else{
				this.wordCount.put(word, 1);
			}
		}
	}

	public int hasWord(String word) {
		for (String key : this.wordCount.keySet()) {
			if (key.equals(word)) {
				return this.wordCount.get(key);
			}
		}
		return 0;
	}
	
	public String getCountData() {
		String str = "";
		for (String key : this.wordCount.keySet()) {
			str+=key+"="+this.wordCount.get(key)+"\n";
		}
		return str;
		
	}
	
	public String readFile(){
		String str = "";
		FileReader fileReader = null;	
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line = buffer.readLine();
			while (line != null) {
				str+=line+" ";	
				line = buffer.readLine();
			}
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		return str;
		
	}
}
