import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class PhoneBook {
	
	private String filename;
	
	public PhoneBook(String filename){
		this.filename = filename;
	}
	
	public String readFile(){
		FileReader fileReader = null;
		String str = "------------ Phone Book ------------\nName\tPhone\n=====\t======\n";
		try {
			fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] data = line.split(",");
				String name = data[0].trim();
				String number = data[1].trim();
				str+=name+"\t#"+number+"\n";			
			}
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		return str;
	}

}
