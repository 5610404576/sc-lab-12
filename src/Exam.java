import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class Exam {

	private String filename;
	private String tofilename;
	
	public void readFrom(String filename){
		this.filename = filename;
	}
	public String readFile(){
		return this.filename;
	}
	public void writeTo(String tofilename){
		this.tofilename = tofilename;
	}
	public String writeFile(){
		return this.tofilename;
	}
	
	public void appendToFile(){
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		try {
			fileReader = new FileReader(readFile());
			BufferedReader buffer = new BufferedReader(fileReader);
			
			fileWriter = new FileWriter(writeFile(),true);
			PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			String line;
			out.println("------------ Exam Scores ------------\nName\tAverage\n=====\t======");
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] data = line.split(",");
				String name = data[0].trim();
				double x1 = Double.parseDouble(data[1].trim());
				double x2 = Double.parseDouble(data[2].trim());
				double grade = (x1+x2)/2;
				out.println(name+"\t"+grade);			
			}
			out.flush();
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				if (fileReader != null){
					fileReader.close();
				}		
				fileWriter.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
}
